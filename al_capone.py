import pandas as pan
import numpy as np
# from pandas import ExcelFile
from sklearn.tree import DecisionTreeRegressor  # Scikit-learn

# Tratamento de entrada do arquivo
filen = "cerveja.xlsx"
dataset = pan.read_excel(filen, sheet_name="Cerveja")

del(dataset["Data"])  # Para simplificar nosso escopo, removemos atributo Data

# Usando Consumo como label
consumo = np.array(dataset["Consumo de cerveja (litros)"])
# print (consumo)
del(dataset["Consumo de cerveja (litros)"])

# Demais dados, usando como atributos
atribs = np.array(dataset)

# Amostras de teste

# Cross validation
test0 = atribs[0]  # 25.461
test1 = atribs[35]  # 19.950
test2 = atribs[202]  # 14.343

# Instancias de teste
test3 = [36.76,	28.1,	40.2,	0,	1]  # Caso com possivel valor rel. alto
test4 = [10.5, 9.5,  11.5,   50.,    0.]  # Caso com possivel valor rel. baixo

# Formato do scikit
test0 = np.array([test0, test1, test2, test3, test4])
# print (test0)

# Usando as funcionalidades do scikit para regressao
regressor = DecisionTreeRegressor(max_depth=5)
regressor.fit(atribs, consumo)  # Treinamento
pred0 = regressor.predict(test0)  # Testando instancias

# Exibicao
print("Temperatura Media (C) Temperatura Minima (C)	Temperatura Maxima (C) Precipitacao (mm) Final de Semana")
cont = 0
while(cont < len(test0)):
    print(list(test0[cont]), " -> Valor da predicao: ", pred0[cont])
    cont += 1
# print(pred0)
